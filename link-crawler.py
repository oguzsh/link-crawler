# Ant
# ! -*- coding: utf-8 -*-
import mechanize, sys, re

br = mechanize.Browser()
br.set_handle_robots(False)
br.addheaders = [('user-agent',
                  'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2.3) Gecko/20100423 Ubuntu/10.04 (lucid) Firefox/3.6.3')]


def information():
    print "[-] Example Usage:"
    print "[+] python link-crawler.py [web site adress]"


try:
    dosya = open("result.txt", "w")
    link = sys.argv[1]
    denetle = link.split(".")
    if re.findall(":", denetle[0]):
        information()
        sys.exit()
    d_link = "http://" + link
    print "Start scanning...  >>  " + link
    br.open(d_link)
    for links in br.links():
        if re.findall(denetle[1], links.url):
            dosya.write(links.url + "\n")
        else:
            dosya.write(d_link + links.url + "\n")
    print "[+] Done. Opening file >> result.txt"

except:
    information()
